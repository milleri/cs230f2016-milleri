#!/usr/bin/env python
#Bryce Evans and Izaak Miller
#CS230 - Final Project
#Turing Machine
#Honor code: The work submitted is our own unless cited otherwise
import Tkinter as Tk
import sys

def write(string):
    tex.config(state=Tk.NORMAL)
    tex.insert("end", string)
    tex.see("end")
    tex.config(state=Tk.DISABLED)

def TM1(testInput, inputLen):
    step = 0
    count = 0
    state = "q0"
    while(state != "q3"): #While we have not reached our goal
        if(state == "q0"):
            print "Step: {},".format(step),
            write("Step: {},   ".format(step))
            step = step + 1
            print "State: q0,",
            write("State: q0, ")
            if(testInput[count] == "0"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("0", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q1"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "x"):
                print "Current input: {},".format(testInput[count])
                write("Current input: {},".format(testInput[count]))
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q3"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1
            else:
                print "Halt! String not accepted."
                write("Halt! String not accepted by language.")
                return

        if(state == "q1"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q1,",
            write("State: q1, ")
            if(testInput[count] == "0"):
                print " Current input: {},".format(testInput[count]),
                write("Current input: {},".format(testInput[count]))
                testInput == testInput[0:count] + testInput[count].replace("0", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q2"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "x"):
                print "Halt! String not accepted."
                return
            else:
                print "Halt! String not accepted."
                return

        if(state == "q2"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q2,",
            write("State: q2, ")
            if(testInput[count] == "x"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {},".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("x", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q3"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "0"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {},".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("0", "0") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                state = "q0"
            else:
                print "Halt! String not accepted."
                write("Halt! String not accepted by language.")
                return

    print "Input accepted by TM1"
    return

def TM2(testInput, inputLen):
    count = 0
    step = 0
    state = "q0"
    while(state != "q3"): #While we have not reached our goal
        if(state == "q0"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q0,",
            write("State: q0, ")
            if(testInput[count] == "0"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("0", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q1"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "1"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("1", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q1"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "x"):
                print "Current input: {},".format(testInput[count])
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q3"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1
            else:
                print "Halt! String not accepted by langauge."
                write("Halt! String not accepted by language.")
                return

        if(state == "q1"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q1,",
            write("State: q1, ")
            if(testInput[count] == "0"):
                print " Current input: {},".format(testInput[count]),
                testInput == testInput[0:count] + testInput[count].replace("0", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q2"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "1"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("1", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q2"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "x"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                print "Halt! String not accepted by language."
                write("Halt! String not accepted by language.")
                return
            else:
                print "Halt! String not accepted by language."
                write("Halt! String not accepted by language.")
                return

        if(state == "q2"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q2,",
            write("State: q2, ")
            if(testInput[count] == "x"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("x", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q3"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "0"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("0", "0") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Action: Staying on this spot"
                state = "q0"
            elif(testInput[count] == "1"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("1", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q3"
                print "Action: Staying on this spot"
            else:
                print "Halt! String not accepted by language."
                write("Halt! String not accepted by language.")
                return

    print "Input accepted by TM2"
    return

def TM3(testInput, inputLen):
    count = 0
    step = 0
    state = "q0"
    while(state != "q12"): #While we have not reached our goal
        if(state == "q0"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q0,",
            write("State: q0, ")
            if(testInput[count] == "0"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("0", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty),
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q1"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "1"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("1", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q8"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "#"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("#", "#") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q12"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            else:
                print "Halt! String not accepted by language."
                write("Halt! String not accepted by language.")
                return
        if(state == "q1"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q1,",
            write("State: q1, ")
            if(testInput[count] == "0"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("0", "0") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q1"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "1"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("1", "1") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q1"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "#"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("#", "#") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q2"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            else:
                print "Halt! String not accepted by language."
                write("Halt! String not accepted by language.")
                return

        if(state == "q2"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q2,",
            write("State: q2, ")
            if(testInput[count] == "0"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("0", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q3"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "x"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("x", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q2"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            else:
                print "Halt! String not accepted by language."
                write("Halt! String not accepted by language.")
                return

        if(state == "q3"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q3,",
            write("State: q3, ")
            if(testInput[count] == "0"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("0", "0") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q3"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "1"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("1", "1") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q3"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "#"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("#", "#") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q4"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            else:
                print "Halt! String not accepted by language."
                write("Halt! String not accepted by language.")
                return

        if(state == "q4"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q4,",
            write("State: q4, ")
            if(testInput[count] == "0"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("0", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q5"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count - 1 #Simulating moving to the left
            elif(testInput[count] == "x"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("x", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q4"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count +1 #Simulating moving to the right
            else:
                print "Halt! String not accepted by language."
                write("Halt! String not accepted by language.")
                return

        if(state == "q5"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q5,",
            write("State: q5, ")
            if(testInput[count] == "0"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("0", "0") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q5"
                print "Action: Moving to the Left"
                write("Action: Moving to the Left\n")
                count = count - 1 #Simulating moving to the left
            elif(testInput[count] == "1"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("1", "1") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q5"
                print "Action: Moving to the Left"
                write("Action: Moving to the Left\n")
                count = count - 1 #Simulating moving to the left
            elif(testInput[count] == "x"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("x", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q5"
                print "Action: Moving to the Left"
                write("Action: Moving to the Left\n")
                count = count - 1 #Simulating moving to the left
            elif(testInput[count] == "#"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("#", "#") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q6"
                print "Action: Moving to the Left"
                write("Action: Moving to the Left\n")
                count = count - 1 #Simulating moving to the left
            else:
                print "Halt! String not accepted by language."
                write("Halt! String not accepted by language.")
                return

        if(state == "q6"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q6,",
            write("State: q6, ")
            if(testInput[count] == "0"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("0", "0") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q6"
                print "Action: Moving to the Left"
                write("Action: Moving to the Left\n")
                count = count - 1 #Simulating moving to the left
            elif(testInput[count] == "1"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("1", "1") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q6"
                print "Action: Moving to the Left"
                write("Action: Moving to the Left\n")
                count = count - 1 #Simulating moving to the left
            elif(testInput[count] == "x"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("x", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q6"
                print "Action: Moving to the Left"
                write("Action: Moving to the Left\n")
                count = count - 1 #Simulating moving to the left
            elif(testInput[count] == "#"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("#", "#") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q7"
                print "Action: Moving to the Left"
                write("Action: Moving to the Left\n")
                count = count - 1 #Simulating moving to the left
            else:
                print "Halt! String not accepted by language."
                write("Halt! String not accepted by language.")
                return

        if(state == "q7"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q7,",
            write("State: q7, ")
            if(testInput[count] == "0"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("0", "0") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q7"
                print "Action: Moving to the Left"
                write("Action: Moving to the Left\n")
                count = count - 1 #Simulating moving to the left
            elif(testInput[count] == "1"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("1", "1") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q7"
                print "Action: Moving to the Left"
                write("Action: Moving to the Left\n")
                count = count - 1 #Simulating moving to the left
            elif(testInput[count] == "x"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("x", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q0"
                print "Action: Moving to the Left"
                write("Action: Moving to the Left\n")
                count = count + 1 #Simulating moving to the left
            else:
                print "Halt! String not accepted by language."
                write("Halt! String not accepted by language.")
                return

        if(state == "q8"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q8,",
            write("State: q8, ")
            if(testInput[count] == "0"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("0", "0") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q8"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "1"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("1", "1") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q8"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "#"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("#", "#") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q9"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            else:
                print "Halt! String not accepted by language."
                write("Halt! String not accepted by language.")
                return

        if(state == "q9"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q9,",
            write("State: q9, ")
            if(testInput[count] == "1"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("1", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q10"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "x"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("x", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q9"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            else:
                print "Halt! String not accepted by language."
                write("Halt! String not accepted by language.")
                return

        if(state == "q10"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q10,",
            write("State: q10, ")
            if(testInput[count] == "0"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("0", "0") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q10"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "1"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("1", "1") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q10"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            elif(testInput[count] == "#"):
                print "Current input: {},".format(testInput[count]),
                write("Current input: {}, ".format(testInput[count]))
                testInput = testInput[0:count] + testInput[count].replace("#", "#") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                print "Tape: {},".format(testInput),
                write("Tape: {}, ".format(testInput))
                state = "q11"
                print "Action: Moving to the Right"
                write("Action: Moving to the Right\n")
                count = count + 1 #Simulating moving to the right
            else:
                print "Halt! String not accepted by language."
                write("Halt! String not accepted by language.")
                return

        if(state == "q11"):
            print "Step: {},".format(step),
            write("Step: {}, ".format(step))
            step = step + 1
            print "State: q11,",
            write("State: q11, ")
            try:
                if(testInput[count] == "1"):
                    print "Current input: {},".format(testInput[count]),
                    write("Current input: {}, ".format(testInput[count]))
                    testInput = testInput[0:count] + testInput[count].replace("1", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                    print "Tape: {},".format(testInput),
                    write("Tape: {}, ".format(testInput))
                    state = "q5"
                    print "Action: Moving to the Left"
                    write("Action: Moving to the Left\n")
                    count = count - 1 #Simulating moving to the left
                elif(testInput[count] == "x"):
                    print "Current input: {},".format(testInput[count]),
                    write("Current input: {}, ".format(testInput[count]))
                    testInput = testInput[0:count] + testInput[count].replace("x", "x") + testInput[(count+1):inputLen] #Replacing 0 with x (empty)
                    print "Tape: {},".format(testInput),
                    write("Tape: {}, ".format(testInput))
                    state = "q11"
                    print "Action: Moving to the Right"
                    write("Action: Moving to the Right\n")
                    count = count + 1 #Simulating moving to the right
                else:
                    print "Halt! String not accepted by language."
                    write("Halt! String not accepted by language.")
                    return
            except Exception as e:
                print "Halt! String not accepted by language."
                write("Halt! String not accepted by language.")
                return

    print "Input accepted by TM3"
    return

root = Tk.Tk()
tex = Tk.Text(master=root)
tex.pack(side=Tk.RIGHT)
bop = Tk.Frame()
bop.pack(side=Tk.LEFT)
Tk.Button(bop, text='Exit', command=root.destroy).pack()

#text_box = Tk.Text(root, state=Tk.DISABLED)
#text_box.grid(row=0, column=0, columnspan=4)

print "Welcome to our Turing Machine!"
write("Welcome to our Turing Machine!\n")
TMchoice = raw_input("Which Turing Machine would you like to use? \nTM1, L = {w | w contains an even number of 0s and w is an element of {0}} \nTM2, L = {ww | w is an element of {0, 1}}\n\
TM3, L = {w#w#w | w is an element of {0,1}}\n")
testInput = raw_input("Input your string: ") + "x"
inputLen = len(testInput)

if TMchoice == "TM1":
    TM1(testInput, inputLen)

elif TMchoice == "TM2":
    TM2(testInput, inputLen)

elif TMchoice == "TM3":
    TM3(testInput, inputLen)

root.mainloop()
